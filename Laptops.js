
const laptopSelection = document.getElementById("laptops");
const laptopSpecs = document.getElementById("specs");
const laptopImage = document.getElementById("img");
const laptopTitle = document.getElementById("title");
const laptopDescription = document.getElementById("description");
const laptopPrice = document.getElementById("price");
const buyNowButton = document.getElementById("buybtn");
let laptops = [];
let laptopBuyPrice = 0;
const ImageLink= "https://noroff-komputer-store-api.herokuapp.com/";

//initialize function
getAllLaptops();
//Gets all the laptops from api and store in array 
async function getAllLaptops(){
    try{
        const respons = await fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
        const data = await respons.json()
        laptops = data;
        addLaptops(laptops); }
    catch(error){
        console.error("could not fetch laptops", error);
    }
}


const addLaptops = (laptops) => {
    laptops.forEach(x => addLaptop(x));
    laptopSpecs.innerText = '';
    laptops[0].specs.forEach(spec => { 
    const laptopSpecsList = document.createElement('Li');
    laptopSpecsList.textContent = spec;
    laptopSpecs.appendChild(laptopSpecsList);});
    laptopImage.src = ImageLink + laptops[0].image;
    laptopTitle.innerText = laptops[0].title;
    laptopDescription.innerText = laptops[0].description;
    laptopPrice.innerHTML = formatter.format(laptops[0].price);
    laptopBuyPrice = laptops[0].price;
}

//Create selction list and add laptop to selection list
const addLaptop = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopSelection.appendChild(laptopElement);
}

//Update information on the selcted laptop
const laptopSelectChange = s => {
    const selectedLaptop = laptops[s.target.selectedIndex];
    laptopSpecs.innerText = '';
    selectedLaptop.specs.forEach(spec => { 
        const laptopSpecsList = document.createElement('Li');
        laptopSpecsList.textContent = spec;
        laptopSpecs.appendChild(laptopSpecsList);});

    laptopImage.src = ImageLink + selectedLaptop.image;
    laptopTitle.innerText = selectedLaptop.title;
    laptopDescription.innerText = selectedLaptop.description;
    laptopPrice.innerHTML = formatter.format(selectedLaptop.price);
    laptopBuyPrice = formatter.format(selectedLaptop.price);
}

//Buys the laptop if there is enough balance
//Decreases count after a computer is bought
function buyNow() {
 if(laptopBuyPrice > Balance) {
    alert("You dont have enough money to buy thi laptop");
 }
 else{
    Balance -= laptopBuyPrice;
    document.getElementById("balance").innerHTML = formatter.format(Balance);;
    alert("You have now bought this laptop");
    loans--;
 }
}

laptopSelection.addEventListener("change", laptopSelectChange);
buyNowButton.addEventListener("click", buyNow);  
