
const loanblance = document.getElementById("outstandingLoan");
let Balance = null;
let OutstandingLoanBalance = null;
let loans = 0;
const loan =  document.getElementById("loan");
loan.style.visibility = "hidden"
let nan = NaN;

 // Number formatter for currency 
const formatter = new Intl.NumberFormat('se-SE', {
  style: 'currency',
  currency: 'SEK',
  // round to whole numbers 
  minimumFractionDigits: 0, 
  maximumFractionDigits: 0,
});

//Takes in a pay amount and adds/uppdates the balance value
function setBalance(balance)
{
  Balance+=balance;
  document.getElementById("balance").innerHTML = formatter.format(Balance);;
  
}

//Takes the pay amount and subtract from the outstanding loan
function PayOutstandingLoan(payLoan)
{
  if(OutstandingLoanBalance == null){setBalance(payLoan)}
  else{
  OutstandingLoanBalance -=payLoan;
  loanblance.innerHTML = OutstandingLoanBalance;
  }
}

//Gives loan to user but checks some conditions befor loan is granted 
//Checks first if user has any loans 
//check if the loan amount is more then double the balance
// Adds loan to outstanding loan, loan count is increased and repay button is available
function getLoan()
{
   try{

   if(loans >= 1){return alert("you cant get another loan befor buying a laptop or paying back your loan")};
 
    let Inputloan = parseInt(prompt("Please enter the amount you want to loan"));
    if(Inputloan > (Balance*2)) 
    {
      return alert("you are not allowed to loan more then double your balance"), 
      OutstandingLoanBalance+= prompt("Please enter the amount you want to loan"),
      loanblance.innerHTML = OutstandingLoanBalance;
    }
    else if (Inputloan !== null){
      OutstandingLoanBalance+=Inputloan;
      loanblance.innerHTML =formatter.format(OutstandingLoanBalance);
      loan.style.visibility = "visible";
      repayloanbtn.style.visibility = "visible"
      loans++
    }
  }
  catch(error)
  {
    return alert("No amount was given, try again")
  }
}

